/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Szonja
 */
public class MyBoard implements Board {

    public static int colCount = 3;
    public static int rowCount = 3;
    public PlayerType[][] board = new PlayerType[colCount][rowCount];

    public MyBoard() {
        for (int i = 0; i < colCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                board[i][j] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        if (rowIdx >= board.length || rowIdx < 0 || colIdx >= board.length || colIdx < 0) {
            throw new CellException(colIdx, rowIdx, "A kérdezett sor vagy oszlop nincs a táblában.");
        }
        return board[colIdx][rowIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {

        if (cell.getCol() < 0 || cell.getCol() > board.length
                || cell.getRow() < 0 || cell.getRow() > board.length) {
            throw new CellException(cell.getCol(), cell.getRow(), "Nem érvényes cella.");
        } else if (!board[cell.getCol()][cell.getRow()].equals(PlayerType.EMPTY)) {
            throw new CellException(cell.getCol(), cell.getRow(), "Itt már van amőba.");
        } else {
            board[cell.getCol()][cell.getRow()] = (cell.getCellsPlayer());
        }

    }

    @Override
    public boolean hasWon(PlayerType p) {

        for (int i = 0; i < colCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                if (board[i][0] == p &&board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                    return true;
                }
                if (board[0][j] == p && board[0][j] == board[1][j] && board[1][j] == board[2][j]) {
                    return true;
                }
                if (board[1][1] == p && board[1][1] == p && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
                    return true;
                }
                if ( board[1][1] == p && board[2][0] == board[1][1] && board[1][1] == board[0][2]) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> c = new ArrayList<>();
        for (int i = 0; i < colCount; i++) {
            for (int j = 0; j < rowCount; j++) {
                Cell cell = new Cell(j, i, PlayerType.EMPTY);
                if (board[i][j].equals(PlayerType.EMPTY)) {
                    c.add(cell);
                }
            }
        }
        return c;
    }
}
