/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.Cell;
import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Szonja
 */
public class simplePlayerImpl extends AbstractPlayer {

    public simplePlayerImpl(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell c = new Cell(j, i);
                try {
                    if (b.getCell(j,i) == PlayerType.EMPTY) {
                        return c;
                    }
                } catch (CellException ex) {
                    Logger.getLogger(simplePlayerImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return null;
    }

    @Override
    public PlayerType getMyType() {
        return PlayerType.X;
    }

}
